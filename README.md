MA5851 Assignment 3 

WebCrawler and NLP System 

Document 1 Overview 

 

Background 

 

Polling in politics has a long history, serving not only to “guess” an election winner, but to examine the issues that matter most to the population, and potentially even guide party decision making (Team Zee Feed, Sept. 2021) 

Almost as long as there has been political polling, there have also been failures in these predictions. Some notable polling failures in recent times have included Brexit, predicted to Remain (Cohn N., June 2016) and the 2016 US presidential elections, predicted towards Clinton (Woodie A., Nov 2016) 

At home we also saw an election polling prediction failure, with the 2019 election being predicted incorrectly as a Labor win. 

 

Political polling has evolved slowly, going from face-to-face, to telephone, and now more digital methods however the author proposes several the recent failures may be due to polling methods not being current enough. People tend to share their views in different ways now, primarily through social media, and are more reluctant to provide honest opinions - perhaps even more so if they are particularly conservative, or “traditional” opinions. 

 

Proposal 

 

This project proposes using the power of Natural Language Processing (NLP) to harness opinion, attempting to create long term, but also evolving indicators of public opinion. 

It’s recognised that this concept has a broad scope, and would constitute a large, multi-faceted project, drawing on a number of different contributors with varied expertise in order to produce a functional, deployable outcome. 

Therefore, I propose several initial steps, which could be improved upon, while also being scalable to project dimensions. 

The first of these is a web scraper, which was set up to extract political news stories from websites. Alongside this a Python package called snscrape is also proposed to extract information from social media sites. 

Following the creation of a corpus of news articles, a number of NLP tasks are possible, and indeed should be investigated. Such tasks might include named entity recognition (NER) or topic modelling and are suggested as further developments 

In this instance I chose to perform keyword recognition analysis as the first NLP task. This allows for a data driven approach to state which matters are currently most topical, and would be interesting to see not only as a snapshot, but also changing over time. 

Building on the results of the first NLP task, it was proposed that social media output be used to undertake sentiment analysis. Ongoing observation of changing sentiment over time, but also across an entire election period should provide invaluable information not only for polling, but for anybody wishing to be on top of current political sentiment. 

 

References 

 

Methods and Accuracy of Political Polls in Australia - Zee Feed 

Why the Surprise Over ‘Brexit’? Don’t Blame the Polls - The New York Times (nytimes.com) 

Six Data Science Lessons from the Epic Polling Failure (datanami.com) 